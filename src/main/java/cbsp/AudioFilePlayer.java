package cbsp;

public class AudioFilePlayer {
    
    public void play(String fileName) {
    	
    	// TASK 1
    	
    	// Initialize File object.
    	
    	// Declare AudioInputStream object.
    	
    	// Declare Clip object
    	
    	// Get AudioInputStream from AudioSystem.
    	
    	// Get DataLine.Info.
    	
    	// Get current Clip Object and open it.
    	
    	// Start the playing of the Clip object.
    	
    	// Wait for the end of the recording.
        
    }
    
    public static void main(String[] args) throws InterruptedException {
        
        String orgFilename="src/main/media/tada.wav";
        //String orgFilename="src/main/media/pcm16/pcm stereo 16 bit 48kHz.wav";
        //String orgFilename="src/main/media/pcm16/pcm mono 16 bit 48kHz.wav";
        //String orgFilename="src/main/media/pcm8/pcm stereo 8 bit 48kHz.wav";
        //String orgFilename="src/main/media/pcm8/pcm mono 8 bit 48kHz.wav";
        //String orgFilename="src/main/media/noise/Brown_Noise.wav";
        
        AudioFilePlayer afp = new AudioFilePlayer();
        System.out.println("Playing the original sound recording.");
        afp.play(orgFilename);
    }
    
}
